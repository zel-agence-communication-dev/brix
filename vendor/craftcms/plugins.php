<?php

$vendorDir = dirname(__DIR__);
$rootDir = dirname(dirname(__DIR__));

return array (
  'craftcms/redactor' => 
  array (
    'class' => 'craft\\redactor\\Plugin',
    'basePath' => $vendorDir . '/craftcms/redactor/src',
    'handle' => 'redactor',
    'aliases' => 
    array (
      '@craft/redactor' => $vendorDir . '/craftcms/redactor/src',
    ),
    'name' => 'Redactor',
    'version' => '2.3.3.2',
    'description' => 'Edit rich text content in Craft CMS using Redactor by Imperavi.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/redactor/blob/v2/README.md',
  ),
  'ether/seo' => 
  array (
    'class' => 'ether\\seo\\Seo',
    'basePath' => $vendorDir . '/ether/seo/src',
    'handle' => 'seo',
    'aliases' => 
    array (
      '@ether/seo' => $vendorDir . '/ether/seo/src',
    ),
    'name' => 'SEO',
    'version' => '3.6.1.1',
    'description' => 'SEO utilities including a unique field type, sitemap, & redirect manager',
    'developer' => 'Ether Creative',
    'developerUrl' => 'https://ethercreative.co.uk',
    'documentationUrl' => 'https://github.com/ethercreative/seo/blob/v3/README.md',
  ),
  'sebastianlenz/linkfield' => 
  array (
    'class' => 'typedlinkfield\\Plugin',
    'basePath' => $vendorDir . '/sebastianlenz/linkfield/src',
    'handle' => 'typedlinkfield',
    'aliases' => 
    array (
      '@typedlinkfield' => $vendorDir . '/sebastianlenz/linkfield/src',
    ),
    'name' => 'Typed link field',
    'version' => '1.0.19',
    'description' => 'A Craft field type for selecting links',
    'developer' => 'Sebastian Lenz',
    'developerUrl' => 'https://github.com/sebastian-lenz/',
  ),
  'verbb/commerce-payflow' => 
  array (
    'class' => 'verbb\\payflow\\Payflow',
    'basePath' => $vendorDir . '/verbb/commerce-payflow/src',
    'handle' => 'commerce-payflow',
    'aliases' => 
    array (
      '@verbb/payflow' => $vendorDir . '/verbb/commerce-payflow/src',
    ),
    'name' => 'PayPal Payflow',
    'version' => '2.0.0',
    'description' => 'PayPal Payflow payment gateway plugin for Craft Commerce 2',
    'developer' => 'Verbb',
    'developerUrl' => 'https://verbb.io',
    'developerEmail' => 'support@verbb.io',
    'documentationUrl' => 'https://github.com/verbb/commerce-payflow',
    'changelogUrl' => 'https://raw.githubusercontent.com/verbb/commerce-payflow/master/CHANGELOG.md',
  ),
  'craftcms/commerce-stripe' => 
  array (
    'class' => 'craft\\commerce\\stripe\\Plugin',
    'basePath' => $vendorDir . '/craftcms/commerce-stripe/src',
    'handle' => 'commerce-stripe',
    'aliases' => 
    array (
      '@craft/commerce/stripe' => $vendorDir . '/craftcms/commerce-stripe/src',
    ),
    'name' => 'Stripe for Craft Commerce',
    'version' => '2.1.0',
    'description' => 'Stripe integration for Craft Commerce 2',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/commerce-stripe/blob/master/README.md',
  ),
  'lukeyouell/craft-stripecheckout' => 
  array (
    'class' => 'lukeyouell\\stripecheckout\\StripeCheckout',
    'basePath' => $vendorDir . '/lukeyouell/craft-stripecheckout/src',
    'handle' => 'stripe-checkout',
    'aliases' => 
    array (
      '@lukeyouell/stripecheckout' => $vendorDir . '/lukeyouell/craft-stripecheckout/src',
    ),
    'name' => 'Stripe Checkout',
    'version' => '2.0.2',
    'description' => 'Bringing the power of Stripe Checkout to your Craft templates.',
    'developer' => 'Luke Youell',
    'developerUrl' => 'https://github.com/lukeyouell/craft-stripecheckout',
    'documentationUrl' => 'https://github.com/lukeyouell/craft-stripecheckout/blob/v2/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/lukeyouell/craft-stripecheckout/v2/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => true,
  ),
  'craftcms/contact-form' => 
  array (
    'class' => 'craft\\contactform\\Plugin',
    'basePath' => $vendorDir . '/craftcms/contact-form/src',
    'handle' => 'contact-form',
    'aliases' => 
    array (
      '@craft/contactform' => $vendorDir . '/craftcms/contact-form/src',
    ),
    'name' => 'Contact Form',
    'version' => '2.2.5',
    'description' => 'Add a simple contact form to your Craft CMS site',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/contact-form/blob/v2/README.md',
    'components' => 
    array (
      'mailer' => 'craft\\contactform\\Mailer',
    ),
  ),
  'rias/craft-contact-form-extensions' => 
  array (
    'class' => 'rias\\contactformextensions\\ContactFormExtensions',
    'basePath' => $vendorDir . '/rias/craft-contact-form-extensions/src',
    'handle' => 'contact-form-extensions',
    'aliases' => 
    array (
      '@rias/contactformextensions' => $vendorDir . '/rias/craft-contact-form-extensions/src',
    ),
    'name' => 'Contact Form Extensions',
    'version' => '1.1.7',
    'schemaVersion' => '1.0.0',
    'description' => 'Adds extensions to the Craft CMS contact form plugin.',
    'developer' => 'Rias',
    'developerUrl' => 'https://rias.be',
    'documentationUrl' => 'https://github.com/riasvdv/craft-contact-form-extensions/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/riasvdv/craft-contact-form-extensions/master/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => true,
    'components' => 
    array (
      'contactFormExtensionsService' => 'rias\\contactformextensions\\services\\ContactFormExtensionsService',
    ),
  ),
  'craftcms/commerce' => 
  array (
    'class' => 'craft\\commerce\\Plugin',
    'basePath' => $vendorDir . '/craftcms/commerce/src',
    'handle' => 'commerce',
    'aliases' => 
    array (
      '@craft/commerce' => $vendorDir . '/craftcms/commerce/src',
    ),
    'name' => 'Craft Commerce',
    'version' => '2.1.12.1',
    'description' => 'An amazingly powerful and flexible e-commerce platform for Craft CMS.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://craftcommerce.com',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://docs.craftcms.com/commerce/v2/',
  ),
);
