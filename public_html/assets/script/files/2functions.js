
                            $('.c-home_slider').slick({
                                infinite: true,
                                slidesToScroll: 1,
                                arrows: false,
                                draggable:true,
                                autoplay: true,
                                autoplaySpeed: 3200,
                                rows:0,
                                responsive: [
                                    {
                                        breakpoint: 99999999999,
                                        settings: {
                                            slidesToShow: 2,
                                            dots: false,
                                        }
                                    },
                                    {
                                        breakpoint: 1280,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1,
                                            dots: true,
                                        }
                                    }
                                ]           
                            });

                        /*----------------------------------------------------------------------
                            BLOCK SEVEN // SHACK'S PAGE
                        ----------------------------------------------------------------------*/           

                            $('.c-block-seven_ul').slick({
                                infinite: true,
                                slidesToScroll: 1,
                                arrows: false,
                                draggable:true,
                                autoplay: true,
                                autoplaySpeed: 3200,
                                rows:0,
                                responsive: [
                                    {
                                    breakpoint: 99999999999,
                                        settings: {
                                            slidesToShow: 2,
                                            dots: false,
                                            }
                                        },
                                    {
                                    breakpoint: 1024,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1,
                                            dots: true
                                        }
                                    }
                                ]           
                            });

                        /*----------------------------------------------------------------------
                            BLOCK THREE // HOME, ABOUT, RECIPES, CONTACT, SHACK PAGES
                        ----------------------------------------------------------------------*/                 
                                
                            $('.c-block-three_slider').slick({
                                infinite: true,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: false,
                                dots: true,
                                draggable:true,
                                rows:0,
                                speed: 500,
                                autoplay: true,
                                autoplaySpeed: 5000,
                                appendDots:'.c-block-three_slider_dots',
                                customPaging : function(slider, i) {
                                    var thumb = $(slider.$slides[i]).data();
                                    var test = i + 1;
                                    return '<a class="dot">'+ 0 + test +'</a>';
                                }
                            });

                        /*----------------------------------------------------------------------
                            BLOCK FIVE // SHACK'S PAGE
                        ----------------------------------------------------------------------*/                     

                            $('.c-list_ul_slider').slick({
                                infinite: true,
                                slidesToScroll: 1,
                                arrows: true,
                                dots: false,
                                rows:0,
                                autoplay: true,
                                autoplaySpeed: 4000,
                                speed: 400,
                                appendArrows: ".c-test",
                                responsive: [
                                    {
                                        breakpoint: 99999999999,
                                        settings: {
                                            slidesToShow: 4,
                                            slidesToScroll: 4,
                                        }
                                    },
                                    {
                                        breakpoint: 960,
                                        settings: {
                                            slidesToShow: 3,
                                            slidesToScroll: 3,
                                        }
                                    },
                                    {
                                        breakpoint: 600,
                                        settings: {
                                            slidesToShow: 2,
                                            slidesToScroll: 2
                                        }
                                    },
                                    {
                                        breakpoint: 400,
                                        settings: {
                                            slidesToShow: 1,
                                            slidesToScroll: 1
                                            
                                        }
                                    }    
                                ]               
                            });     

                    /*--------------------------------------------------------------------------
                        SOCIAL MEDIA POPUP
                    --------------------------------------------------------------------------*/

                        /*----------------------------------------------------------------------
                            FACEBOOK
                        ----------------------------------------------------------------------*/      
                        
                            $(document).ready(function() {
                                $('.fb-share').click(function(e) {
                                    e.preventDefault();
                                    window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                                    return false;
                                });
                            });

                        /*----------------------------------------------------------------------
                            TWITTER
                        ----------------------------------------------------------------------*/     

                            $(document).ready(function() {
                                $('.tw-share').click(function(e) {
                                    e.preventDefault();
                                    window.open($(this).attr('href'), 'twShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
                                    return false;
                                });
                            });

                    /*--------------------------------------------------------------------------
                        FORM
                    --------------------------------------------------------------------------*/

                        /*----------------------------------------------------------------------
                            SUBMIT
                        ----------------------------------------------------------------------*/  

                            $('#c-form').submit(function(ev) {
                                ev.preventDefault();
                                var validator = $( "#c-form" ).validate();
                                if(validator.form() == true) {
                                    $.post({
                                        url: '/',
                                        dataType: 'json',
                                        data: $(this).serialize(),
                                        success: function(response) {
                                            if (response.success) {
                                                $('#c-form').find("input[type=text], textarea, select, input[type=email], input[type=select], input[type=radiobutton],input[type=file] ").val("");
                                                $('label.invalid').remove();
                                                $('.c-form_input').removeClass('valid has-content');
                                                $('#c-form').addClass('confirmation');
                                            }
                                        }
                                    });
                                }
                            });




$(document).ready(function() {
  $("#c-inscription").validate();
  $('#current-password').val('');
});

        $("#c-product_select").change(function() {

        // var selectedVal = $("#c-product_select option:selected").text();
        var selectedVal = $("#c-product_select option:selected").attr('data-price');
        var selectedFormat = $("#c-product_select option:selected").attr('data-format');
        $('.c-product-single_price').html(selectedVal);
        $('.c-product-single_size.-editable').html(selectedFormat);

    });


                        /*----------------------------------------------------------------------
                            REMOVE CONFIRMATION 
                        ----------------------------------------------------------------------*/      

                            $( ".c-form_input" ).keyup(function() {
                                $('#c-form').removeClass('confirmation');
                            });

                        /*----------------------------------------------------------------------
                            EFFECTS ON LABEL
                        ----------------------------------------------------------------------*/  

                                $(".c-form_field .c-form_input").focusout(function(){
                                    if($(this).val() != "" || $(this).hasClass('-email')){
                                        $(this).addClass("has-content");
                                    }else{
                                        $(this).removeClass("has-content");
                                    }
                                })

                                $(window).on('load', function(){
                                    $('.c-form_input').each(function(){
                                        if($(this).val() != "" ){
                                            $(this).addClass("has-content");
                                        }else{
                                            $(this).removeClass("has-content");
                                        }
                                    })
                                })                                

                        /*----------------------------------------------------------------------
                            MASK PHONE
                        ----------------------------------------------------------------------*/              

                            $("#phone").inputmask({"mask": "(999) 999-9999"});

                         /*----------------------------------------------------------------------
                            VALIDATOR PLUGIN
                        ----------------------------------------------------------------------*/  

                            if($('.lang-en').length) { // ENGLISH
                                $("#c-form").validate({
                                    rules: {
                                        fromName : {
                                        required: true
                                        },
                                        Phone: {
                                            required: true,
                                            phoneUS: true
                                        },
                                        fromEmail: {
                                            required: true,
                                            email: true
                                        },
                                        message: {
                                            required: true          
                                        }
                                    },
                                    messages : {
                                        fromName: {
                                            required: "Please enter your full name",
                                        },
                                        Phone: {
                                            required: "Please enter your phone number",
                                            phoneUS: "Please specify a valid phone number"
                                        },
                                        fromEmail: {
                                            required: "Please enter your email",
                                            email: "The email should be in the format: abc@domain.tld"
                                        },
                                        message: {
                                            required: "Please enter your message",
                                        },  
                                    }       
                                });
                            } else { // FRENCH
                                $("#c-form").validate({
                                    rules: {
                                        fromName : {
                                            required: true
                                        },
                                        Phone: {
                                            required: true,
                                            minlength: 7,
                                            phoneUS: true,
                                        },
                                        fromEmail: {
                                            required: true,
                                            email: true
                                        },
                                        message: {
                                            required: true          
                                        }
                                    },
                                    errorClass : 'invalid',
                                    success: function(label){
                                        label.addClass("valid");    
                                    },
                                    messages : {
                                        fromName: {
                                            required: "Veuillez entrer votre nom complet",
                                        },
                                        Phone: {
                                            required: "Veuillez entrer votre numéro de téléphone",
                                            phoneUS: "Veuillez spécifier un numéro de téléphone valide",
                                            minlength: "Veuillez spécifier un numéro de téléphone valide"
                                        },
                                        fromEmail: {
                                            required: "Veuillez entrer votre adresse courriel",
                                            email: "Le courriel doit être au format: abc@domain.tld"
                                        },
                                        message: {
                                            required: "Veuillez entrer votre message",
                                        },  
                                    }       
                                }); 
                                  }
  $("#c-inscription").validate({
                                    rules: {
                                        fromName : {
                                            required: true
                                        },
                                        Phone: {
                                            required: true,
                                            minlength: 7,
                                            phoneUS: true,
                                        },
                                        email: {
                                            required: true,
                                            email: true
                                        },
                                        message: {
                                            required: true          
                                        }
                                    },
                                    errorClass : 'invalid',
                                    success: function(label){
                                        label.addClass("valid");    
                                    },
                                    messages : {
                                        fromName: {
                                            required: "Veuillez entrer votre nom complet",
                                        },
                                        Phone: {
                                            required: "Veuillez entrer votre numéro de téléphone",
                                            phoneUS: "Veuillez spécifier un numéro de téléphone valide",
                                            minlength: "Veuillez spécifier un numéro de téléphone valide"
                                        },
                                        email: {
                                            required: "Veuillez entrer votre adresse courriel",
                                            email: "Le courriel doit être au format: abc@domain.tld"
                                        },
                                        message: {
                                            required: "Veuillez entrer votre message",
                                        },  
                                    }       
                                }); 

 var currentEmail = $('#email').val();


$("#profile").validate({
                                    rules: {
                                        username : {
                                            required: true
                                        },                                        
                                        Phone: {
                                            required: true,
                                            minlength: 7,
                                            phoneUS: true,
                                        },
                                        email: {
                                            required: true,
                                            email: true
                                        },
                                        message: {
                                            required: true          
                                        },
    password: {
        required: {
            depends: function() {
                return ($('#new-password').val().length >= 1 || $('#email').val() !== currentEmail);
            },
}
    },                                        
                                    },
                                    errorClass : 'invalid',
                                    success: function(label){
                                        label.addClass("valid");    
                                    },
                                    messages : {
                                        username: {
                                            required: "Veuillez entrer votre nom complet",
                                        },                                        
                                        Phone: {
                                            required: "Veuillez entrer votre numéro de téléphone",
                                            phoneUS: "Veuillez spécifier un numéro de téléphone valide",
                                            minlength: "Veuillez spécifier un numéro de téléphone valide"
                                        },
                                        email: {
                                            required: "Veuillez entrer votre adresse courriel",
                                            email: "Le courriel doit être au format: abc@domain.tld"
                                        },
                                        message: {
                                            required: "Veuillez entrer votre message",
                                        },  
 password: {
        required:  "Required to change Password and Email."
    },                                        
                                    }       
                                }); 



      $(window).on('load',function(){
$("#profile").validate({
                                    rules: {
                                        firstName : {
                                            required: true
                                        },
                                        lastName : {
                                            required: true
                                        },                                        
                                        Phone: {
                                            required: true,
                                            minlength: 7,
                                            phoneUS: true,
                                        },
                                        email: {
                                            required: true,
                                            email: true
                                        },
                                        message: {
                                            required: true          
                                        },
                                    },
                                    errorClass : 'invalid',
                                    success: function(label){
                                        label.addClass("valid");    
                                    },
                                    messages : {
                                        firstName: {
                                            required: "Veuillez entrer votre nom complet",
                                        },
                                        lastName: {
                                            required: "Veuillez entrer votre nom complet",
                                        },                                        
                                        Phone: {
                                            required: "Veuillez entrer votre numéro de téléphone",
                                            phoneUS: "Veuillez spécifier un numéro de téléphone valide",
                                            minlength: "Veuillez spécifier un numéro de téléphone valide"
                                        },
                                        email: {
                                            required: "Veuillez entrer votre adresse courriel",
                                            email: "Le courriel doit être au format: abc@domain.tld"
                                        },
                                        message: {
                                            required: "Veuillez entrer votre message",
                                        },  
                                    }       
                                }); 
if($("#profile").length) {
  $("#profile").validate().form();
}
                            });




                    /*--------------------------------------------------------------------------
                        MOBILE OR DESKTOP
                    --------------------------------------------------------------------------*/

                        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            $('html').addClass('js-mobile');
                        } else {
                            $('html').addClass('js-desktop');
                        }            
                              
                    /*--------------------------------------------------------------------------
                        SCROLL MAGIC
                    --------------------------------------------------------------------------*/

                        var controller = new ScrollMagic.Controller({globalSceneOptions: {duration: 1000000}});
                        new ScrollMagic.Scene({
                            triggerHook: 'onEnter',
                            triggerElement: ".c-block-three_wrap",
                            duration: $('.c-block-three_wrap').height()
                        })

                        .setClassToggle(".c-block-three_second", "is-inview") // add class toggle
                        .addTo(controller);

                    /*--------------------------------------------------------------------------
                        MENU - MOBILE
                    --------------------------------------------------------------------------*/

                        /*----------------------------------------------------------------------
                            HAMBURGER MENU
                        ----------------------------------------------------------------------*/   

                            var hamburger = document.querySelector(".hamburger");
                            hamburger.addEventListener("click", function(e) {
                                $('html').toggleClass('-js-menu-active');
                                hamburger.classList.toggle("is-active");
                                e.stopPropagation();
                            });

                            $(".c-mobile-nav").click(function(e){
                                e.stopPropagation();
                            });

                            $('.c-main').click(function(){
                                $('html').removeClass('-js-menu-active');
                                $('.hamburger').removeClass("is-active");
                            });

                        /*----------------------------------------------------------------------
                            EVENTS KEYDOWN
                        ----------------------------------------------------------------------*/  

                            $(document).on('keydown', function(event) {
                                                                    if(!$('.slick-lightbox').is(':visible')) {
                                if($(window).innerWidth() < 840) {

                                        if (event.key == "Escape") {
                                            $('html').toggleClass('-js-menu-active');
                                            $('.hamburger').toggleClass("is-active");
                                        }
                                    }
                                }
                            });

                            $(document).on('keydown', function(event) {
                                if($(window).innerWidth() < 840) {
                                    if (!$(".c-form_input").is(":focus")) {
                                        if (event.keyCode == 77) {
                                            $('html').addClass('-js-menu-active');
                                            $('.hamburger').addClass("is-active");
                                        }
                                    }
                                }
                            });

                        /*----------------------------------------------------------------------
                            RESIZE
                        ----------------------------------------------------------------------*/              

                            $(window).on('resize', function() {
                                if($(window).innerWidth() > 839) {
                                    $('html').removeClass('-js-menu-active');
                                    $('.hamburger').removeClass("is-active");
                                }
                            })

                    /*--------------------------------------------------------------------------
                        NAVIGATION 
                    --------------------------------------------------------------------------*/
                        
                        var main = new Headroom(document.querySelector(".c-header_primary-menu"), {
                            tolerance: 5,
                            offset : 0,
                            classes: {
                              initial: "animated",
                              pinned: "slideInDown",
                              unpinned: "slideOutUp"
                            }
                        });
                        main.init();

                    /*--------------------------------------------------------------------------
                        LIGHT BOX
                    --------------------------------------------------------------------------*/

                        $('.c-list_ul_slider').slickLightbox({
                            itemSelector: ' li  a'
                        });



$('.c-user').on('click', function() {
    $(this).find('ul').slideToggle();
})


$(window).on('scroll', function() {
$('.c-user ul').slideUp(100);
                });


$('.c-user').on('click', function() {
    $('.c-user_options').slideToggle();
})


//parallax
!function(t,i,e,s){function o(i,e){var h=this;"object"==typeof e&&(delete e.refresh,delete e.render,t.extend(this,e)),this.$element=t(i),!this.imageSrc&&this.$element.is("img")&&(this.imageSrc=this.$element.attr("src"));var r=(this.position+"").toLowerCase().match(/\S+/g)||[];if(r.length<1&&r.push("center"),1==r.length&&r.push(r[0]),"top"!=r[0]&&"bottom"!=r[0]&&"left"!=r[1]&&"right"!=r[1]||(r=[r[1],r[0]]),this.positionX!==s&&(r[0]=this.positionX.toLowerCase()),this.positionY!==s&&(r[1]=this.positionY.toLowerCase()),h.positionX=r[0],h.positionY=r[1],"left"!=this.positionX&&"right"!=this.positionX&&(isNaN(parseInt(this.positionX))?this.positionX="center":this.positionX=parseInt(this.positionX)),"top"!=this.positionY&&"bottom"!=this.positionY&&(isNaN(parseInt(this.positionY))?this.positionY="center":this.positionY=parseInt(this.positionY)),this.position=this.positionX+(isNaN(this.positionX)?"":"px")+" "+this.positionY+(isNaN(this.positionY)?"":"px"),navigator.userAgent.match(/(iPod|iPhone|iPad)/))return this.imageSrc&&this.iosFix&&!this.$element.is("img")&&this.$element.css({backgroundImage:"url("+this.imageSrc+")",backgroundSize:"cover",backgroundPosition:this.position}),this;if(navigator.userAgent.match(/(Android)/))return this.imageSrc&&this.androidFix&&!this.$element.is("img")&&this.$element.css({backgroundImage:"url("+this.imageSrc+")",backgroundSize:"cover",backgroundPosition:this.position}),this;this.$mirror=t("<div />").prependTo(this.mirrorContainer);var a=this.$element.find(">.parallax-slider"),n=!1;0==a.length?this.$slider=t("<img />").prependTo(this.$mirror):(this.$slider=a.prependTo(this.$mirror),n=!0),this.$mirror.addClass("parallax-mirror").css({visibility:"hidden",zIndex:this.zIndex,position:"fixed",top:0,left:0,overflow:"hidden"}),this.$slider.addClass("parallax-slider").one("load",function(){h.naturalHeight&&h.naturalWidth||(h.naturalHeight=this.naturalHeight||this.height||1,h.naturalWidth=this.naturalWidth||this.width||1),h.aspectRatio=h.naturalWidth/h.naturalHeight,o.isSetup||o.setup(),o.sliders.push(h),o.isFresh=!1,o.requestRender()}),n||(this.$slider[0].src=this.imageSrc),(this.naturalHeight&&this.naturalWidth||this.$slider[0].complete||a.length>0)&&this.$slider.trigger("load")}!function(){for(var t=0,e=["ms","moz","webkit","o"],s=0;s<e.length&&!i.requestAnimationFrame;++s)i.requestAnimationFrame=i[e[s]+"RequestAnimationFrame"],i.cancelAnimationFrame=i[e[s]+"CancelAnimationFrame"]||i[e[s]+"CancelRequestAnimationFrame"];i.requestAnimationFrame||(i.requestAnimationFrame=function(e){var s=(new Date).getTime(),o=Math.max(0,16-(s-t)),h=i.setTimeout(function(){e(s+o)},o);return t=s+o,h}),i.cancelAnimationFrame||(i.cancelAnimationFrame=function(t){clearTimeout(t)})}(),t.extend(o.prototype,{speed:.2,bleed:0,zIndex:-100,iosFix:!0,androidFix:!0,position:"center",overScrollFix:!1,mirrorContainer:"body",refresh:function(){this.boxWidth=this.$element.outerWidth(),this.boxHeight=this.$element.outerHeight()+2*this.bleed,this.boxOffsetTop=this.$element.offset().top-this.bleed,this.boxOffsetLeft=this.$element.offset().left,this.boxOffsetBottom=this.boxOffsetTop+this.boxHeight;var t,i=o.winHeight,e=o.docHeight,s=Math.min(this.boxOffsetTop,e-i),h=Math.max(this.boxOffsetTop+this.boxHeight-i,0),r=this.boxHeight+(s-h)*(1-this.speed)|0,a=(this.boxOffsetTop-s)*(1-this.speed)|0;r*this.aspectRatio>=this.boxWidth?(this.imageWidth=r*this.aspectRatio|0,this.imageHeight=r,this.offsetBaseTop=a,t=this.imageWidth-this.boxWidth,"left"==this.positionX?this.offsetLeft=0:"right"==this.positionX?this.offsetLeft=-t:isNaN(this.positionX)?this.offsetLeft=-t/2|0:this.offsetLeft=Math.max(this.positionX,-t)):(this.imageWidth=this.boxWidth,this.imageHeight=this.boxWidth/this.aspectRatio|0,this.offsetLeft=0,t=this.imageHeight-r,"top"==this.positionY?this.offsetBaseTop=a:"bottom"==this.positionY?this.offsetBaseTop=a-t:isNaN(this.positionY)?this.offsetBaseTop=a-t/2|0:this.offsetBaseTop=a+Math.max(this.positionY,-t))},render:function(){var t=o.scrollTop,i=o.scrollLeft,e=this.overScrollFix?o.overScroll:0,s=t+o.winHeight;this.boxOffsetBottom>t&&this.boxOffsetTop<=s?(this.visibility="visible",this.mirrorTop=this.boxOffsetTop-t,this.mirrorLeft=this.boxOffsetLeft-i,this.offsetTop=this.offsetBaseTop-this.mirrorTop*(1-this.speed)):this.visibility="hidden",this.$mirror.css({transform:"translate3d("+this.mirrorLeft+"px, "+(this.mirrorTop-e)+"px, 0px)",visibility:this.visibility,height:this.boxHeight,width:this.boxWidth}),this.$slider.css({transform:"translate3d("+this.offsetLeft+"px, "+this.offsetTop+"px, 0px)",position:"absolute",height:this.imageHeight,width:this.imageWidth,maxWidth:"none"})}}),t.extend(o,{scrollTop:0,scrollLeft:0,winHeight:0,winWidth:0,docHeight:1<<30,docWidth:1<<30,sliders:[],isReady:!1,isFresh:!1,isBusy:!1,setup:function(){function s(){if(p==i.pageYOffset)return i.requestAnimationFrame(s),!1;p=i.pageYOffset,h.render(),i.requestAnimationFrame(s)}if(!this.isReady){var h=this,r=t(e),a=t(i),n=function(){o.winHeight=a.height(),o.winWidth=a.width(),o.docHeight=r.height(),o.docWidth=r.width()},l=function(){var t=a.scrollTop(),i=o.docHeight-o.winHeight,e=o.docWidth-o.winWidth;o.scrollTop=Math.max(0,Math.min(i,t)),o.scrollLeft=Math.max(0,Math.min(e,a.scrollLeft())),o.overScroll=Math.max(t-i,Math.min(t,0))};a.on("resize.px.parallax load.px.parallax",function(){n(),h.refresh(),o.isFresh=!1,o.requestRender()}).on("scroll.px.parallax load.px.parallax",function(){l(),o.requestRender()}),n(),l(),this.isReady=!0;var p=-1;s()}},configure:function(i){"object"==typeof i&&(delete i.refresh,delete i.render,t.extend(this.prototype,i))},refresh:function(){t.each(this.sliders,function(){this.refresh()}),this.isFresh=!0},render:function(){this.isFresh||this.refresh(),t.each(this.sliders,function(){this.render()})},requestRender:function(){var t=this;t.render(),t.isBusy=!1},destroy:function(e){var s,h=t(e).data("px.parallax");for(h.$mirror.remove(),s=0;s<this.sliders.length;s+=1)this.sliders[s]==h&&this.sliders.splice(s,1);t(e).data("px.parallax",!1),0===this.sliders.length&&(t(i).off("scroll.px.parallax resize.px.parallax load.px.parallax"),this.isReady=!1,o.isSetup=!1)}});var h=t.fn.parallax;t.fn.parallax=function(s){return this.each(function(){var h=t(this),r="object"==typeof s&&s;this==i||this==e||h.is("body")?o.configure(r):h.data("px.parallax")?"object"==typeof s&&t.extend(h.data("px.parallax"),r):(r=t.extend({},h.data(),r),h.data("px.parallax",new o(this,r))),"string"==typeof s&&("destroy"==s?o.destroy(this):o[s]())})},t.fn.parallax.Constructor=o,t.fn.parallax.noConflict=function(){return t.fn.parallax=h,this},t(function(){t('[data-parallax="scroll"]').parallax()})}(jQuery,window,document);



                        if($('.c-block-four_bg').length) {
                        $('.c-block-four_bg').parallax({mirrorContainer:".barba-container"});
}


if($('.c-hero_banner').length) { 
            var controller = new ScrollMagic.Controller();

            new ScrollMagic.Scene({
                triggerHook: 'onLeave',
                triggerElement: '.c-header_banner',
                duration: $('.c-hero_banner').height()
            })

            .setTween(TweenMax.to('.c-hero_banner', 1, {y: '16%', ease: Linear.easeNone}))
            .addTo(controller);     
}
            if($('.c-home_slider').length) { 
            var controller = new ScrollMagic.Controller();

            new ScrollMagic.Scene({
                triggerHook: 'onLeave',
                triggerElement: '.c-header_banner',
                duration: $('.c-home_slider').height()
            })

            .setTween(TweenMax.to('.c-home_slider .slick-slide', 1, {y: '16%', ease: Linear.easeNone}))
            .addTo(controller);     
}


    $(document).ready(function() {
            $('.minus').click(function () {
                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();
                return false;
            });
            $('.plus').click(function () {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });
        });

$('.c-menus').on('click', function() {
     $(this).find('ul').slideToggle();
     $(this).toggleClass('-js-menus-active');
});






                    /*--------------------------------------------------------------------------
                        GOOGLE MAPS
                    --------------------------------------------------------------------------*/

                        if($('#c-google-maps').length) {
                            $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyBw0owXetwxncGlySjky80tO73LsJ879N8").done(function(script, textStatus) {
                                var mapAction = function(CurrentAnimation) {
                                    var center_ico = new google.maps.LatLng(45.037509, -73.990703);     
                                    var locations = [['Excavation L. Martel', 45.037509, -73.990703]];
                                    // Create an array of styles.
                                    var styles = [ { "featureType": "all", "elementType": "labels.text.fill", "stylers": [ { "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 } ] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [ { "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 } ] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [ { "color": "#000000" }, { "lightness": 20 } ] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [ { "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 } ] }, { "featureType": "administrative", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.country", "elementType": "geometry", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.country", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.province", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.locality", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.neighborhood", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#000000" }, { "lightness": 20 } ] }, { "featureType": "landscape", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#000000" }, { "lightness": 21 } ] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#000000" }, { "lightness": 17 } ] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 } ] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "color": "#000000" }, { "lightness": 18 } ] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [ { "color": "#000000" }, { "lightness": 16 } ] }, { "featureType": "transit", "elementType": "geometry", "stylers": [ { "color": "#000000" }, { "lightness": 19 } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#000000" }, { "lightness": 17 } ] } ]
                                    var styledMap = new google.maps.StyledMapType(styles,
                                    {name: 'Styled Map'});
                                    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                        var mapOptions = {zoom: 11,center: center_ico,zoomControl: true,scrollwheel: false,draggable: false,mapTypeControl: false,zoomControlOptions: {style: google.maps.ZoomControlStyle.MEDIUM,position: google.maps.ControlPosition.LEFT_CENTER},streetViewControl: false,panControl: false,panControlOptions: {position: google.maps.ControlPosition.LEFT_CENTER}}
                                    } else {
                                        var mapOptions = {zoom: 11,center: center_ico,zoomControl: true,scrollwheel: false,draggable: true,fullscreenControl: false,mapTypeControl: false,zoomControlOptions: {style: google.maps.ZoomControlStyle.MEDIUM,position: google.maps.ControlPosition.LEFT_CENTER},streetViewControl: false,panControl: false,panControlOptions: {position: google.maps.ControlPosition.LEFT_CENTER}}
                                    }      
                                    var map = new google.maps.Map(document.getElementById('c-google-maps'),mapOptions);
                                    marker = new google.maps.Marker({map:map,animation: google.maps.Animation.DROP,size: new google.maps.Size(20, 32)});
                                    map.mapTypes.set('map_style', styledMap);
                                    map.setMapTypeId('map_style');
                                    var infowindow = new google.maps.InfoWindow();
                                    var marker, i;
                                    for (i = 0; i < locations.length; i++) {  
                                        var image = {
                                            scaledSize: new google.maps.Size(32, 49),
                                            // url: "https://staging-valens.agencezel.dev/assets/images/icons/fermes-valens_shop.svg"
                                        };
                                        marker = new google.maps.Marker({
                                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                            map: map,
                                            // ,icon: image,
                                            optimized: false,
                                            animation: CurrentAnimation
                                        });
                                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                            return function() {
                                            infowindow.setContent(locations[i][0]);
                                            }
                                        })(marker, i));
                                    }
                                }
                                window.onload = mapAction();
                                $(window).resize(function() {
                                    mapAction();
                                });  
                            });
                        } 